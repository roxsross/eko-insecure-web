# Gestión de Vulnerabilidades con DefectDojo

## Introducción

En este artículo, presento la plataforma DefectDojo, una solución de código abierto dedicada a la gestión de vulnerabilidades que también ofrece una serie de otras características relacionadas con la seguridad. Proporcionaré detalles técnicos sobre la implementación de DevSecOps y mostraré cómo desplegar DefectDojo con Docker. Además, se incluirá una breve teoría sobre la Gestión de Vulnerabilidades y algunos consejos para implementar este proceso de la manera más efectiva.

## ¿Qué es la Gestión de Vulnerabilidades?

Antes de presentar las capacidades de DefectDojo para el seguimiento de vulnerabilidades, aclaremos lo que entendemos por el término Gestión de Vulnerabilidades. La siguiente definición es la más adecuada en mi opinión:

> La Gestión de Vulnerabilidades es el proceso continuo y regular de identificar, evaluar, informar, gestionar y remediar vulnerabilidades de seguridad en puntos finales, cargas de trabajo y sistemas.

Esta definición muestra claramente que la Gestión de Vulnerabilidades, como muchos otros procesos relacionados con la ciberseguridad, no es una tarea única, sino un proceso continuo.

Las vulnerabilidades pueden identificarse de diferentes maneras, comenzando por evaluaciones de seguridad internas manuales, pasando por equipos de red, pruebas de penetración, programas de recompensas por errores y terminando con soluciones automatizadas que pueden identificar y reportar hallazgos casi en tiempo real. Para rastrear estos hallazgos de manera efectiva, necesitamos una plataforma que permita recopilar y seguir todos ellos a lo largo de su ciclo de vida, desde la identificación del problema hasta su resolución. Dicha plataforma puede implementarse de diversas formas. Un enfoque común es utilizar una solución de gestión de tareas y tratar cada hallazgo como un ticket separado. Es el enfoque más rentable y eficiente en términos de implementación y gestión. Sin embargo, para implementar el proceso más efectivo con muchas integraciones, se pueden considerar soluciones como DefectDojo.

## Introducción a DefectDojo

Según la página del repositorio de GitHub de DefectDojo:

> DefectDojo es una plataforma de orquestación de seguridad y gestión de vulnerabilidades. DefectDojo te permite gestionar tu programa de seguridad de aplicaciones, mantener información de productos y aplicaciones, clasificar vulnerabilidades y enviar hallazgos a sistemas como JIRA y Slack.

Comparando la descripción de DefectDojo con nuestra definición de Gestión de Vulnerabilidades, parece muy prometedor. En términos de aspectos técnicos, DefectDojo es una aplicación web desarrollada con Python y Django. Utiliza una base de datos relacional SQL donde se puede configurar PostgreSQL o MySQL. También utiliza Celery para algunas tareas programadas como la deduplicación automatizada de hallazgos y la sincronización con JIRA. Además, RabbitMQ (o Redis) se utiliza como broker de mensajes para la ejecución asincrónica. Estos componentes se presentan en el diagrama de arquitectura a continuación:

![Arquitectura de DefectDojo](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/architecture_diagram.png)

Sabemos qué es DefectDojo y cómo está construido, veamos un resumen de lo que ofrece:

- Seguimiento de vulnerabilidades y compromisos de seguridad listo para usar.
- Seguimiento de productos/componentes con varios detalles.
- Métricas personalizadas con paneles.
- Benchmarks y cuestionarios para productos rastreados.
- API.

## Despliegue de DefectDojo

DefectDojo se puede desplegar con Docker Compose. Para el despliegue, se sugiere tener al menos 2 vCPUs, 8GB de RAM y 2GB de espacio en disco. Sin embargo, los 2GB de espacio en disco sugeridos no incluyen el almacenamiento de la base de datos y este valor debe considerarse antes del despliegue de la base de datos.

Para los propósitos de este artículo, estoy desplegando DefectDojo localmente con Docker Compose. Es necesario instalar Docker y Docker Compose para replicar mis instrucciones. Primero, el repositorio de DefectDojo debe clonarse con el siguiente comando:

```bash
git clone https://github.com/DefectDojo/django-DefectDojo.git
```

Ahora, cambiemos el directorio a django-DefectDojo, configuremos las variables de entorno y despleguemos DefectDojo en el perfil postgres-redis con los siguientes comandos:

```bash
# Cambiar directorio al repositorio clonado
cd django-DefectDojo

# Configurar variables de entorno necesarias para ejecutar los contenedores
./docker/setEnv.sh

# Ejecutar contenedores con Docker Compose Up usando configuraciones preconfiguradas
./dc-up.sh postgres-redis

# Use docker compose logs -f initializer to track its progress.
docker compose logs initializer | grep "Admin password:"

```

Nota importante:
Deben tener instalado compose y compose plugin o en su defecto ir por tu instalacion en kubernetes
```bash
DC_VERSION=$(curl -L -s -H 'Accept: application/json' https://github.com/docker/compose/releases/latest | sed -e 's/.*"tag_name":"\([^"]*\)".*/\1/')
sudo curl -L "https://github.com/docker/compose/releases/download/$DC_VERSION/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo mkdir -p /usr/local/lib/docker/cli-plugins/
sudo curl -SL https://github.com/docker/compose/releases/latest/download/docker-compose-linux-x86_64 -o /usr/local/lib/docker/cli-plugins/docker-compose
sudo chmod +x /usr/local/lib/docker/cli-plugins/docker-compose 
```

DefectDojo ofrece algunos perfiles con diferentes enfoques de despliegue. El script anterior utiliza PostgreSQL y Redis como bases de datos. Pero, si prefieres MySQL como base de datos relacional, también se puede usar especificando mysql-redis en lugar de posgres-redis. Además, redis puede ser reemplazado por rabbitmq.

Después de esperar unos momentos, DefectDojo se despliega localmente en [http://localhost:8080/](http://localhost:8080/). El usuario administrador se crea por el script de inicialización en el primer despliegue si el usuario adminuser no existe. La contraseña para este usuario se genera automáticamente y puede observarse en los logs filtrando por el patrón Admin password:. Estas credenciales pueden usarse para iniciar sesión en la aplicación web por primera vez.

![Página de Inicio de Sesión de DefectDojo](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/login.png)

Después de ingresar el nombre de usuario y la contraseña, deberías observar el panel de control de DefectDojo:

![Panel de Control de DefectDojo](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/dashboard.png)

En las siguientes secciones, describo aspectos específicos de trabajar con DefectDojo en varios ejemplos.

## Despliegue de DefectDojo en Producción

Para fines de producción, sugiero usar el script `dc-up-d.sh` desde el directorio raíz del proyecto y ajustar los valores en el archivo `docker-compose.yml`. El contenido de este archivo debe revisarse cuidadosamente y modificarse, especialmente en términos de secretos. Además, después del despliegue solo la aplicación web debe estar expuesta externamente. La base de datos y otros servicios no deben estar expuestos externamente para conexiones entrantes.

## Productos, Componentes, Puntos Finales y Hallazgos

DefectDojo permite a los usuarios crear productos y componentes. Cada producto puede tener una serie de componentes. Un hallazgo de seguridad puede asociarse con un componente y debe estar vinculado a un producto. Un producto puede crearse haciendo clic en Productos desde el menú de la izquierda → Icono de herramientas en la esquina superior derecha → Agregar producto. Este procedimiento se presenta a continuación:

![Añadir un Producto en DefectDojo](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/add_product.png)

El producto contiene varios campos de datos como:

- Nombre del producto,
- Descripción,
- Gerente del producto (un usuario),
- Contacto técnico (un usuario),
- Gerente del equipo (un usuario),
- Tipo de producto (que debe agregarse antes de crear un producto),
- Criticidad empresarial,
- Plataforma,
- Ciclo de vida,
- …y más.

Cuando se crea un producto, se puede acceder a él desde el menú Productos. La siguiente captura de pantalla muestra cómo se ve una página de producto para un producto recién creado:

![Página del Producto de DefectDojo](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/product.png)

Cuando se crea el producto, también se pueden agregar nuevas vulnerabilidades. Nuevas vulnerabilidades pueden crearse desde una página de producto después de hacer clic en Hallazgos → Agregar nuevo hallazgo o creando un nuevo compromiso haciendo clic en Compromisos → Agregar nuevo […] compromiso. Utilicé el primer método y el formulario se presenta a continuación:

![Agregar un Hallazgo Ad Hoc con DefectDojo](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/add_finding.png)

Cada hallazgo tiene varios campos que pueden completarse. Basado en mi experiencia, contiene todos los campos cruciales que son útiles para la gestión de vulnerabilidades. Puedes agregar descripción, pasos para reproducir, vector y puntuación CVSS, severidad, nombre del componente y su versión y mucho más. Tal vez el único campo que encontré que falta es el Sistema de Puntuación de Predicción de Explotación (EPSS). Sobre EPSS puedes leer en mi otro artículo:

![Lista de Hallazgos Abiertos](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/open_findings.png)

Como puedes observar, en la lista anterior, el hallazgo “Server Side Request Forgery” está afectando al “Producto Insignia”. Desde esta lista cada hallazgo puede cerrarse como Falso Positivo, Duplicado, Fuera de Alcance o puede pasar por un proceso de aceptación de riesgo. Desde mi experiencia, esta lista sería el lugar más visitado al gestionar vulnerabilidades.

Además, cuando agregas un nombre de componente al crear un hallazgo, el componente se creará automáticamente y se agregará a la sección Componentes. De esta manera, creé un “Componente Principal” del “Producto Insignia” creado anteriormente.

Por último, pero no menos importante,

 los hallazgos pueden subirse en un solo archivo que contenga un informe. Muchos escáneres de seguridad están integrados con DefectDojo y sus archivos de salida pueden cargarse directamente en DefectDojo. Para la lista completa de escáneres compatibles, consulta [la lista de escáneres compatibles](https://github.com/DefectDojo/django-DefectDojo/tree/master/dojo/tools).

## Métricas y Cuadros de Mando

Como se mencionó anteriormente, DefectDojo ofrece varios cuadros de mando que visualizan estadísticas sobre hallazgos rastreados. Primero, revisemos el cuadro de mando de producto. La siguiente captura de pantalla presenta este cuadro de mando con datos de ejemplo de un producto rastreado.

![Cuadro de Mando de Producto](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/product_dashboard.png)

Como puedes observar, hay varias estadísticas útiles sobre el producto. Tal vez, las más útiles sean las métricas basadas en el tiempo que visualizan hallazgos por su gravedad, área y la cantidad total de hallazgos para el producto seleccionado.

Además, DefectDojo ofrece el cuadro de mando de métricas que puede utilizarse para crear cuadros de mando más específicos:

![Cuadro de Mando de Métricas](https://raw.githubusercontent.com/DefectDojo/django-DefectDojo/master/docs/images/metrics_dashboard.png)

Como puedes observar, hay varios gráficos basados en el tiempo que visualizan varias estadísticas basadas en los hallazgos. La parte inferior del cuadro de mando contiene un panel de consulta SQL que puede utilizarse para obtener gráficos más específicos.

## Integraciones

Las integraciones son uno de los mayores beneficios de DefectDojo. La mayoría de las soluciones de seguridad corporativas ya están integradas con DefectDojo o permiten la creación de scripts personalizados basados en la API. La lista completa de herramientas compatibles puede encontrarse en [la página de la herramienta](https://github.com/DefectDojo/django-DefectDojo/tree/master/dojo/tools).

Además, DefectDojo tiene una integración incorporada con JIRA. Para activar la integración, primero, debe configurarse correctamente JIRA en el servidor DefectDojo y la tarea de sincronización de JIRA debe ejecutarse en un intervalo específico (por ejemplo, todos los días a la medianoche). Para la configuración, consulta [la página de JIRA](https://defectdojo.github.io/django-DefectDojo/integrations/jira/).

Además, una integración de Slack es compatible y DefectDojo ofrece webhooks que pueden utilizarse para enviar eventos a Slack. Para una guía paso a paso, consulta [la guía de integración de Slack](https://defectdojo.github.io/django-DefectDojo/integrations/slack/).

## Conclusión

DefectDojo ofrece muchas funcionalidades cruciales para la Gestión de Vulnerabilidades y seguridad corporativa. Es un excelente punto de partida para implementar un programa de Gestión de Vulnerabilidades. Además, la aplicación puede personalizarse fácilmente según sea necesario. La personalización se ofrece de varias maneras, comenzando por la configuración de bases de datos externas y terminando con el uso de paneles de consulta SQL. Además, DefectDojo tiene un gran soporte comunitario que puede ayudarte con las configuraciones más específicas.

Sin embargo, ten en cuenta que, aunque DefectDojo ofrece muchas funcionalidades, debe integrarse adecuadamente con otras herramientas de seguridad que puedan identificar vulnerabilidades. Esto permite el proceso de Gestión de Vulnerabilidades más efectivo.