# Guía de Inicio Rápido para DefectDojo

## Introducción

DefectDojo es una plataforma de orquestación de seguridad y gestión de vulnerabilidades. Esta guía rápida te ayudará a comenzar con DefectDojo utilizando la demo pública.

## Acceso a la Demo

Puedes acceder a la demo de DefectDojo utilizando las siguientes credenciales:

- **URL de Inicio de Sesión**: [https://demo.defectdojo.org/login?next=/](https://demo.defectdojo.org/login?next=/)
- **Usuario**: `admin`
- **Contraseña**: `1Defectdojo@demo#appsec`

Más información sobre cómo comenzar con la demo pública se encuentra en el siguiente enlace:
- **Guía de Inicio**: [https://defectdojo.github.io/django-DefectDojo/getting_started/demo/](https://defectdojo.github.io/django-DefectDojo/getting_started/demo/)

## Crear Productos

Para comenzar a utilizar DefectDojo, primero debes crear un producto. Un producto representa una aplicación o sistema que deseas monitorear.

1. **URL para Crear Productos**: [https://demo.defectdojo.org/product](https://demo.defectdojo.org/product)
2. Completa el formulario con la información del producto y haz clic en "Save Product".

## Crear Engagements

Una vez que hayas creado un producto, puedes crear un engagement. Los engagements representan actividades de seguridad, como pruebas de penetración o auditorías de seguridad, realizadas sobre un producto.

## Recursos Adicionales

- **Documentación Oficial**: [DefectDojo Documentation](https://defectdojo.github.io/django-DefectDojo/)
- **Repositorio de GitHub**: [D
